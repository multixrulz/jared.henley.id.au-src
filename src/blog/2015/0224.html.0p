#!/usr/bin/python

Replacements(
    title = """Python in the Browser: Skulpt""",
    description = """Today I'm looking at [skulpt](http://www.skulpt.org/)""",

    )

WebPage(Markdown(
"""
# Python in the Browser: Skulpt

Today I’m looking at [skulpt](http://www.skulpt.org/).

## What is it, exactly

From the website:

Skulpt is an entirely in-browser implementation of Python.

No preprocessing, plugins, or server-side support required, just write Python and reload.
____________

The skulpt site allows you to enter your own Python source code and run it right on the page!

Activity on github is regular and recent, so skulpt appears to be an ongoing concern.

## Licence

Skulpt may be licensed under:

1. The MIT license.
2. Or, for compatibility with Python, the PSFLv2.
____________

The MIT licence allows anything, as long as the copyright notice and licence continue to be distributed.

## Installation

Installation is as easy as loading up the skulpt javascript files into your web page.

## Online examples

The online examples are pretty cool, and the performance is good.  [These examples](http://www.skulpt.org/static/proctest.html) in particular look like it’s very easy to get fancy results.  That page runs fairly well on my phone too, although the touch interaction with the distanceFun example was not very good.

## Learning Curve

Unfortunately, user documentation is quite lousy, so getting the script below to execute took quite some work.  OK, so it was partly my own issues, as AsciiDoc swallowed the Sk.configure line because it wanted to interpret the braces.

```
<script src="skulpt.min.js" type="text/javascript"></script>
<script type="text/javascript">
// output functions are configurable.  This one just appends some text
// to a pre element.
function outf(text) {{
    var mypre = document.getElementById("pythonstdout");
    mypre.innerHTML = mypre.innerHTML + text;
}}
// Here’s everything you need to run a python program in skulpt
// configure the output function
// call Sk.importMainWithBody()
function runit() {{
    var code="print(’Hello World’)";
    Sk.configure(\{{output:outf\}});
    try {{
      eval(Sk.importMainWithBody("<stdin>",false,code));
    }}
    catch(e) {{
       alert(e.toString())
    }}
}}

</script>

<pre id="pythonstdout">Output appears here
</pre>
<button type="button" onclick="runit()">Run</button>
```

<script src="skulpt/skulpt.min.js" type="text/javascript"></script>
<script type="text/javascript">
// output functions are configurable.  This one just appends some text
// to a pre element.
function outf(text) {{
    var mypre = document.getElementById("pythonstdout");
    mypre.innerHTML = mypre.innerHTML + text;
}}
// Here’s everything you need to run a python program in skulpt
// get a reference to your pre element for output
// configure the output function
// call Sk.importMainWithBody()
function runit() {{
    var code="print(’Hello World’)";
    Sk.configure(\{{output:outf\}});
    try {{
      eval(Sk.importMainWithBody("<stdin>",false,code));
    }}
    catch(e) {{
       alert(e.toString())
    }}
}}

</script>

<pre id="pythonstdout">Output appears here
</pre>
<button type="button" onclick="runit()">Run</button>

Getting your python code into skulpt is a challenge if you want it to be embedded in the web page.  This is because javascript doesn’t know how to do multiline strings, so you have to embed `\n\` at the end of every line.

## Coverage of the Python standard library

Spotty, but improving.  However, the lousyness of the documentation (ie non-existent) makes programming in Skulpt a great challenge.

## Programming graphics

Graphics programming exists, although I’m not sure how good it is at this stage.  Skulpt can draw onto a HTML canvas, so it should be quite capable.

## Conclusion

Skulpt looks like it’s quite capable.  It has the ability to run python scripts entirely browser-side and do graphics and interaction, so it wins there.  Where it loses big time is in its lack of documentation.
""")
)
