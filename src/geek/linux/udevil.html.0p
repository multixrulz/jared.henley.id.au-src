#!/usr/bin/python

Replacements(
    title = """Udevil &amp; systemd""",
    description = """Setting up udevil with systemd to replace udevmnt""",

    )

WebPage(Markdown(
"""
# Udevil &amp; systemd

Systemd broke my USB mounting scripts!  Fortunately udevil and devmon are a wonderful replacement.

The way I like USB mounting to work is as follows:

* USB storage devices are mounted at /media/filesystem-label
* Mountpoints are created with the "storage" group, to allow access to all users in that group.
* All users get write access to the mount point
* I can specify the mount options I want, especially FAT-based filesystems and NTFS (for fmask/dmask and filename conversion)
* Devices get mounted when nobody’s logged in.

Udevil already does a lot of this already with its default configuration.  Here’s what I did to finish the job.

*Allow write access to members of the mountpoint group (ie storage group)*

    - In /etc/udevil/udevil.conf change the fmask and dmask options to +fmask=0002+ and +dmask=0002+ for the filesystems that have them specified.
    - In /etc/udevil/udevil.conf change the allowed fmask and dmask options to +fmask=0002+ and +dmask=0002+
    - In /etc/udevil/udevil.conf change the mount point mode to 0775

*Devmon is shipped with udevil, and it comes with a systemd unit file.*

First, create a devmon user to run devmon under.

```
# useradd -g storage -M -u 300 devmon
# usermod -L devmon
```

Enable and start devmon (the unit file is named devmon@ and requires a username after the @)

```
# systemctl enable devmon@devmon
# systemctl start devmon@devmon
```
""")
)
