#!/usr/bin/python

from zcms import *
import datetime
import markdown

class Breadcrumbs():
    def __init__(self, dirname):
        self._trail = [{'dirname': dirname, 'fsdir': ""}]

    def subdir(self, dirname, fsdir):
        self._trail.append({'dirname': dirname, 'fsdir': fsdir})

    def mdlist(self):
        fspath = ''
        mdlist = ''
        for t in self._trail:
            fspath += t['fsdir'] + "/"
            mdlist += ("* [{}]({})\n".format(t['dirname'], fspath))
        return mdlist

    def html(self):
        return markdown.markdown(self.mdlist())

Markdown.default_extensions(['fenced_code', 'smarty', 'extra'])

directoryname = "Jared Henley"
bc = Breadcrumbs(directoryname)
breadcrumbs = bc.html()

Replacements(
    title = '',
    colour = 'gold',
    description = '',
    directoryname = directoryname,
    picture = "sunflower.png",
    mtime_format = "%A, %B %e, %Y at %H:%M:%S",
    now_format = "%A, %B %e, %Y at %H:%M:%S",
    breadcrumbs = breadcrumbs
)

footerinfo = markdown.markdown("""
**This website is Copyright &copy; 2005-2020 Jared Henley**

Built with [0cms](http://0cms.henley.id.au).

All content is licenced [CC-BY-ND](http://creativecommons.org/licenses/by-nd/4.0/) unless otherwise noted.  Code is licenced under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html). [Credits](/credits.html)

_Last built: {now}_

_Last modified: {mtime}_
""")

Template("""<!DOCTYPE html>
<html>
<head>
<title>{title}</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="/css/layout.css">
<link rel="stylesheet" type="text/css" href="/css/{colour}.css">
<link rel="shortcut icon" href="/favicon.png">
<meta name="description" content="{description}">
<meta name="generator" content="0cms">
<meta name="author" content="Jared Henley">
<meta name="copyright" content="&copy; {year} Jared Henley">
<meta name="verify-v1" content="sOYDFF7yuYP5c4zAAejxDlHXwprCPvTXQ6CoRodicuQ=">
</head>
<body>
<div id="width">
    <div id="head">
        <div id="hd2">
            <img class="stretch" src="/img/headbg_{colour}.png" alt="background">
            <p>{directoryname}</p>
            <img id="topimg" src="/img/{picture}" alt="nice pic">
        </div>
    </div>
    <div id="menu">
        <ul>
            <li><a href="/" title="Jared Henley's Website">Home</a></li>
            <li><a href="/blog" title="Jared Henley's blog">Blog</a></li>
            <li><a href="/geek/" title="Geeky Stuff">Geek</a></li>
            <li><a href="/stuff/" title="Other Stuff">Stuff</a></li>
            <li><a href="/software/" title="Software">Software</a></li>
        </ul>
    </div>
    <div id="trail">
{breadcrumbs}
    </div>
    <div id="content">
{document}
    </div>
    <div id="foot">

<form id="searchform" action="https://duckduckgo.com/" method="get">
<input id="searchform_q" name="_q" type="text" value="" />
<input id="searchformq" name="q" type="hidden" value="" />
<input type="button" value="Search" onclick="search()" />
<script>
function search() {{
    document.getElementById("searchformq").value =
        document.getElementById("searchform_q").value
        + " site:jared.henley.id.au"
    document.getElementById("searchform").submit()
}}
</script>
</form>

{footerinfo}
</div>
</div>
</body>
</html>
""".replace('{footerinfo}', footerinfo))
#!/usr/bin/python

directoryname = "Geeky Stuff"
bc.subdir(directoryname, "geek")
breadcrumbs = bc.html()

Replacements(
    directoryname = directoryname,
    breadcrumbs = breadcrumbs
)
