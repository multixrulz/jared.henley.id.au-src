#!/usr/bin/python

Replacements(
    title = """Lesson 4: Pictures""",
    description = """How to put pictures into your web pages."""
    )

WebPage(Markdown(
"""
# Lesson 4: Pictures

By now you probably want to include some pictures in your pages.  This is really very simple.

```
<img src="picture.jpg" alt="A lovely sunset"
    title="Wish you were here" height="200" width="300">
```

This is the first tag you’ve seen that has attributes.  Attributes are optional (except for src).  Here’s what the attributes mean:

src
:   The picture you want to show.  You can use JPEG, PNG or GIF.  This can be a full URL ("http://www.something.com/dir/pic.jpg"), or just a name.  It could also be the name of a picture in a different directory.  More on that later.

alt
:   Some text to display when the picture isn’t available (eg. it hasn’t loaded yet).  Many browsers display this if you move the mouse over the picture.

title
:   A title for the picture.  Generally this isn’t displayed at all.

height
:   The height of the picture.

width
:    The width of the picture.

If the height or width attributes are supplied, the picture will be displayed with that height or width, regardless of the actual size of the picture.

## Paths

What do you do if the picture you want to use isn’t in the same directory (folder) as your HTML file?  You can use the path to the picture.

```
<img src="pictures/flower.jpg">
```

Looks for a directory called “pictures” (in the same directory as the HTML file), and finds the file called “flower.jpg” in that.  You can go down as many directories as you like.

```
<img src="dir1/dir2/dir3/dir4/dir5/pic.png">
```

```
<img src="../dog.jpg">
```

“..” is a special directory.  It is the parent of the current directory (ie. the directory that contains the current directory).

```
<img src="/images/smiley.png">
```

Until now we’ve been using relative paths -- they are relative to the HTML file we’re using.  Absolute paths start at the top of the website (or filesystem) you’re working in.  So if your website is located at “http://www.groovy.net/”, the picture referred to above is at “http://www.groovy.net/images/smiley.png”.  This is handy if you have some pictures that you use all the time, but you obviously don’t want to copy them around everywhere, or try to remember how many “..” you need to use to get back to the top of your website.

## Practice

* Make a page displaying your favourite photos.

[Previous Lesson](lesson3.html)
[Index](index.html)
[Next Lesson](lesson5.html)
""")
)
