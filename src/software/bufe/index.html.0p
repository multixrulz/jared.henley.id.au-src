#!/usr/bin/python

Replacements(
    title = """bufe""",
    description = """bufe makes it easy to run backups""",
    icon_Download_bufe = HtmlImage('/img/icons/download.png', alt='Download_bufe icon', htmlclass="icon")
    )

WebPage(Markdown(
"""
# bufe

bufe is a program that lets you specify a few parameters for a backup and then run it easily.  It supports mirrors using rsync, incremental backups using rdup, and a disk-based incremental backup that uses rync’s hard-linking feature.

Bufe is a major reason that my data is backed up -- it’s so easy to use on a day-to-day basis that the job actually gets done.

[{icon_Download_bufe} Download bufe](bufe-20140406.tar.bz2)

## Dependencies

* rsync
* rdup
* cdrkit (for dirsplit)
* tar

## Usage

```
Usage: bufe {{-l | backup_name}}
```

`bufe -l` lists all the backups defined in ~/.config/bufe/bufe.conf.

`bufe backup_name` runs the named backup defined in ~/.config/bufe/bufe.conf.

```
[rsync_backup]
type = mirror
source = /path/to/original/
destination = /path/to/mirror
preserve = True
```

A mirror backup runs rsync to mirror the source directory to the destination directory.  Normal rsync rules about trailing slashes apply.  Preserve passes the ’a’ option to rsync.  If preserve is false, options ’rltD’ are passed to rsync instead.

```
[rdup_backup]
type = incremental
source = /path/to/original
destination = /path/to/backup
tmp = /tmpdir
splitsize = 600M
```

An incremental backup uses rdup to back up only modified files (based on ctime).  The files are split using dirsplit into equal-sized groups if possible, which are then tarred.

## Give back

If you find this program useful, [give something back](/software/giveback.html).
""")
)
