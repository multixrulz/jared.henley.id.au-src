#!/usr/bin/python

Replacements(
    title = """Wildproxy &mdash; execute a command over multiple files""",
    description = """Wildproxy allows you to run a program that takes only a single input file on multiple files, ie it allows you to use wildcards.""",
    icon_Download = HtmlImage('/img/icons/download.png', alt='Download icon', htmlclass="icon")
    )

WebPage(Markdown(
"""
# Wildproxy &mdash; execute a command over multiple files

[{icon_Download} Download](wildproxy-20120607.tar.bz2)

Some programs only allow you to supply one input file.  Examples include `pdf2djvu`, and imagemagick’s `convert`.  But what if you want to run that program on lots of files?  You could:

* Sit at the computer for several minutes/hours entering the same command for different input files.
* Bang together a little script to do it for you.
* Use wildproxy.

Wildproxy basically takes the commandline you want to run and the list of files you want to run it on, and then executes that commandline on every input file.  It’s smart enough to let you substitute the filename without its extension too.

```
usage: wildproxy ’command to execute’ filenames
```

The command to be executed should be enclosed in single quotes.  It understands the following replacements:

* !f -- the filename of the file being operated on
* !b -- the basename (the part before the extension) of the file being operated on
* !e -- the extension of the file being operated on (with the dot, if an extension is present)

The choice of ! will cause problems if your command or any of the files have a ! in them, but this doesn’t seem very likely.

## Give back

If you find this program useful, [give something back](/software/giveback.html).
""")
)
