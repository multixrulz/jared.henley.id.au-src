#!/usr/bin/python

Replacements(
    title = """Templates""",
    description = """Each directory in the source tree may contain a configuration file named template.""",

    )

WebPage(Markdown(
"""
# Templates

Each directory in the source tree may contain a configuration file named template.conf.  This file contains the page templates to be used by awb.  If a template is not specified, very basic defaults are provided.

Here is an example of template.conf:

```
[main template]
This template is the first you would typically define.

Typically you would put here some stuff that you want
to insert before the main text of your page, like a
navigation bar.

Then you insert the page content like this:

<?insert content?>

Then you can write your footer.

If you need special formatting like <div> or <table>,
remember that you can add these using the methods that
AsciiDoc provides.

[sub template]
This template replaces <?insert content?> from the main
template.  Use this when you want a subdirectory to add
something to the main template inherited from the parent
without overriding it completely.

[post template]
When you write a blog, this template is applied to each
blog post.  It replaces <?insert content?> in the sub
template.

[room template]
When you write a gallery room, this template is applied
to it.  It replaces <?insert content?> in the sub
template.

[room photo template]
This template is applied to each photo in a gallery room.

[photo template]
This template replaces <?insert content?> in the sub
template for each photo-individual file in a gallery.
```

awb templates are cascading.  This allows templates to be defined once in the root of the website, and applied everywhere.
""")
)
