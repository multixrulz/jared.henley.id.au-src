#!/usr/bin/python

import re
import datetime

month_names = {1: 'January',
    2: 'February',
    3: 'March',
    4: 'April',
    5: 'May',
    6: 'June',
    7: 'July',
    8: 'August',
    9: 'September',
    10: 'October',
    11: 'November',
    12: 'December'}

# Compile an re to replace non-numeric chars
nondigit = re.compile("\D")

def path_to_date(path):
    # Strip off the file extension
    datestr = path.replace('.html.0p', '')
    # Remove all non-numeric chars
    datestr = nondigit.sub('', datestr)
    # Interpret the filename as a date.  If it's not a date, ignore it.
    try:
        date = datetime.datetime.strptime(datestr, "%Y%m%d")
    except ValueError:
        try:
            date = datetime.datetime.strptime(datestr, "%Y%m%d%H")
        except ValueError:
            try:
                date = datetime.datetime.strptime(datestr, "%Y%m%d%H%M")
            except ValueError:
                try:
                    date = datetime.datetime.strptime(datestr, "%Y%m%d%H%M%S")
                except ValueError:
                    date = None
    return date
